#include <iostream>
#include <limits>
#include "primeNumbers.hpp"

int main()
{
    int number;
    std::string input;
    while (true)
    {
        std::cout << "Введите целое неотрицательное число: ";
        std::cin >> input;
        bool isValid = true;
        for (char c : input)
        {
            if (!std::isdigit(c))
            {
                isValid = false;
                break;
            }
        }
        if (isValid)
        {
            number = std::stoi(input);
            break;
        }
        else
        {
            std::cout << "Ошибка ввода: введите целое неотрицательное число." << std::endl;
        }
    }

    std::cout << (isPrime(number) ? "Число простое" : "Число составное") << std::endl;
    return 0;
}